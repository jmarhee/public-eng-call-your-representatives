Call Your Representatives
===

An application to provide browser-based calls to members of congress via Twilio. The operator can fill up a Twilio account, enter their credentials into the app's environmental variables when starting the app, and run this site to provide a free way for their community (via libraries, and other public computers) to contact Congress.

**Note** This is a very rudimentary app, and I am not a web developer, so I'd like to open this up for others to contribute to and adopt to service this initiative!

Setup 
---

Create a Twilio number, and locate your [Twilio sid and API token](twilio.com/user/account) for your application to be able to create calls from this appli
cation.


Build & Run
---

Can be built on any Docker host using this Dockerfile:

```
docker build -t callyourreps .
```

and run:

```
docker run -d -p 80:4567 \
-e numbers_outbound="1234567890" \
-e twilio_sid="" \
-e twilio_token="" \
--name call-your-reps callyourreps
```

to start the application using your outbound Twilio numbers.

