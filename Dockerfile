FROM ruby:2.2.0
MAINTAINER Joseph D. Marhee <joseph@marhee.me>

ADD ./app/ /root/app/

WORKDIR /root/app/

ENV numbers_outbound ""
ENV twilio_sid ""
ENV twilio_token "" 

RUN bundle install 

ENTRYPOINT ruby app.rb -o 0.0.0.0
